toolbox = 'FieldTripBufferMatlab'; if isempty( which([toolbox '/buffer'])),addpath(fullfile(fileparts(which(mfilename)),toolbox));end

host = 'localhost';
port = 1972;

% read the header for the first time to determine number of channels and sampling rate
hdr = buffer( 'get_hdr', [], host, port );

nReadOperations = 0;
latestSampleRead = 0;
batchSizeInSeconds = 0.5;
batchSizeInSamples = batchSizeInSeconds * hdr.fsample;

plotHandle = [];

while true
	% determine number of samples available in buffer
	hdr = buffer( 'get_hdr', [], host, port );

	% see whether new samples are available
	latestSampleAvailable = hdr.nsamples - 1;
	numberOfNewSamplesAvailable = latestSampleAvailable - latestSampleRead;

	if numberOfNewSamplesAvailable >= batchSizeInSamples

		% determine the samples to process
		firstSampleToRead = latestSampleRead + 1;
		lastSampleToRead  = latestSampleRead + batchSizeInSamples;

		delaySeconds = ( latestSampleAvailable - lastSampleToRead ) / hdr.fsample;	
		if delaySeconds > batchSizeInSeconds  % If we've fallen too far behind,
			% then catch up, at the cost of skipping samples.
			% This gets the very most recent (batchSizeInSeconds) amount of data,
			% even if there's a gap between that and the last batch we saw:
			fprintf( 'skipping ahead by %gs\n', delaySeconds );
			lastSampleToRead  = latestSampleAvailable;
			firstSampleToRead = lastSampleToRead - batchSizeInSamples + 1;
		end

		nReadOperations = nReadOperations + 1;
		fprintf( 'processing segment %d from %gs to %gs\n', nReadOperations, firstSampleToRead / hdr.fsample, lastSampleToRead / hdr.fsample );

		% read data segment from buffer
		data = buffer( 'get_dat', [ firstSampleToRead lastSampleToRead ], host, port );

		% remember up to where the data was read
		latestSampleRead = lastSampleToRead;

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% subsequently the data can be processed, here it is only plotted
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		% create a matching time-axis
		time = ( firstSampleToRead:lastSampleToRead ) / hdr.fsample;

		% plot the data just like a standard FieldTrip raw data strucute
		if isempty( plotHandle )
			plotHandle = plot( time, data.buf' );
			figure( gcf )
		else
			% Unfortunately we can't do things the simple way and just reissue
			% plot() commands indefinitely:  matlab eventually chokes,
			% presumably due to memory leaks or similar issues in its plotting
			% routines. But we can update the xdata & ydata of existing handles:
			set( plotHandle, 'xdata', time );
			set( plotHandle, { 'ydata' }, num2cell( data.buf, 2 ) )
		end
		% ensure tight axes
		xlim( [ time(1) time(end) ] );

		% force Matlab to update the figure
		drawnow

	end % if new samples available
	pause( 0.1 )
end % while true
