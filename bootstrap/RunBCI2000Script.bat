:: Call from your BCI2000 script file as  RunBCI2000Script %0 %* #!
:: For example:
::
:: #!/usr/bin/env DYLIB_LIBRARY_PATH=../prog ../prog/BCI2000Shell#
:: @cls & RunBCI2000Script %0 %* #!
:: show window; warn Hello world! Welcome to $BCI2000LAUNCHDIR
::
:: This tool will then look for the path to a BCI2000 distribution in the following order,
:: and pass the BCI2000 script that invoked it to the prog\BCI2000Shell.exe interpreter in
:: the resulting location:
::
:: 1 - the directory specified by the content of a one-line text file called .BCI2000, located
::     in the same directory as the calling BCI2000 script file (watch out for trailing
::     whitespace on that one line)
::
:: 2 - the directory specified by the content of an environment variable %BCI2000%
::
:: 3 - the parent directory of the directory in which this tool resides. If it is kept in a
::     subdirectory of the BCI2000 distro, such as batch, then this is equivalent to the
::     previous convention in which the #! line had ..\prog\BCI2000Shell hard-coded. It
::     makes sense to keep RunBCI2000Script.bat inside the batch directory itself (so it
::     can be found automatically by the BCI2000 scripts next to it when they are
::     double-clicked). Alternatively, it can be put into another subdirectory---but then
::     that subdirectory would need to be put onto the system PATH (note that this makes
::     the BCI2000 distro itself less "portable" in the sense that it now relies on that
::     bit of system configuration outside of its own directory tree)

@echo off
setlocal

set "STARTDIR=%CD%
set "WHEREAMI=%~0\..
where "%~0" >NUL 2>NUL && for /f "usebackq tokens=*" %%a in (`where "%~0"`) do set WHEREAMI=%%a\..
cd /d "%WHEREAMI%"
set "WHEREAMI=%CD%
cd "%STARTDIR%"
::echo %WHEREAMI%

set "SIGNPOST=%~1\..\.BCI2000
if exist "%SIGNPOST%" for /f "usebackq tokens=*" %%a in (`type "%SIGNPOST%"`) do set BCI2000=%%a

if "%BCI2000%"=="" set "BCI2000=%WHEREAMI%\..

set "SHELL=%BCI2000%\prog\BCI2000Shell.exe
if not exist "%SHELL%" echo could not find "%SHELL%" && exit /b 1
"%SHELL%" %* && exit /b 0 || exit /b 1

endlocal
