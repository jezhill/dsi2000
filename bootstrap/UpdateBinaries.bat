@set "DST=%~0\..\..
@set "STARTDIR=%CD%
@cd "%DST%"
@set "DST=%CD%
@cd "%STARTDIR%"

@set "SRC=%~1
@set "DEFAULTSRC=C:\bci2000-svn\HEAD
@if "%SRC%"=="" set /p SRC=Enter path to BCI2000 distribution (default is %DEFAULTSRC%): 
@if "%SRC%"=="" set SRC=%DEFAULTSRC%

copy /Y "%SRC%\prog\BCI2000Shell.exe"             "%DST%\prog\"
copy /Y "%SRC%\prog\Operator.exe"                 "%DST%\prog\"
copy /Y "%SRC%\prog\OperatorLib.dll"              "%DST%\prog\"
copy /Y "%SRC%\prog\SignalGenerator.exe"          "%DST%\prog\"
copy /Y "%SRC%\prog\DSISerial.exe"                "%DST%\prog\"
copy /Y "%SRC%\prog\FilePlayback.exe"             "%DST%\prog\"
copy /Y "%SRC%\prog\DummySignalProcessing.exe"    "%DST%\prog\"
copy /Y "%SRC%\prog\FieldTripBuffer.exe"          "%DST%\prog\"
copy /Y "%SRC%\prog\SpectralSignalProcessing.exe" "%DST%\prog\"
copy /Y "%SRC%\prog\P3SignalProcessing.exe"       "%DST%\prog\"
copy /Y "%SRC%\prog\P3Speller.exe"                "%DST%\prog\"
copy /Y "%SRC%\prog\StimulusPresentation.exe"     "%DST%\prog\"
copy /Y "%SRC%\prog\CursorTask.exe"               "%DST%\prog\"
copy /Y "%SRC%\prog\DummyApplication.exe"         "%DST%\prog\"

::set "BCPY2000=%SRC%\src\contrib\BCPy2000\demo\prog
::for %%b in (PythonSource,PythonSignalProcessing,PythonApplication) do if exist "%SRC%\prog\%%b.exe" ( copy /Y "%SRC%\prog\%%b.exe" "%DST%\prog\" ) else ( copy /Y "%BCPY2000%\%%b.exe" "%DST%\prog\" )

@pause
