﻿Setting up DSI headsets (as at 2016-08-03 —most current DSI_API version: 1.13.1)


In wireless mode:

1.  Make sure you're using a Toshiba Bluetooth dongle rather than a Motorola one.
    If your dongle is branded as Toshiba, great.  If it's branded as Cirago, then
    you'll need to look at with a magnifying glass, look for a model number, and
    determine whether it contains the number 6210 (Toshiba chip inside) or 6310
    (Motorola chip inside). Some headsets were shipped with 6310s, but Wearable
    Sensing has found that some of their connectivity problems were related to
    the 6310's Motorola chip (and/or its associated software), whereas the 6210's
    Toshiba chip and associated software performed better.

2.  You will need the driver at
    http://www.cirago.com/drivers/bta6210-driver-v2_1-win7-64bit.zip  (accessible
    from http://www.cirago.com/bta6210.php under the DOWNLOAD section). This will
    install the Toshiba Bluetooth software stack. Note that this can only be used
    with Toshiba dongles—otherwise the stack will be considered a “trial” version
    and will expire. Fortunately, recent versions of the Toshiba Bluetooth stack
    can now be installed alongside other Bluetooth systems without compromising
    them. 

3.  During installation, the Toshiba installer will ask you to insert the
    dongle.  Choose carefully which USB port you are going to dedicate to this
    dongle: you will have to stick to it in future.

4.  Following installation, a restart is required; Toshiba will ask for
    permission to complete some admin tasks, and then a window will pop up telling
    you how to pair devices - click “do not show this window again” and dismiss
    it.

5.  You will now have an additional Bluetooth icon in the system tray (a
    lighter-blue “Bluetooth Manager” as opposed to the darker-blue “Bluetooth
    Devices” icon that you may or may not have as a built-in part of the Windows
    system). For the DSI we’ll be using the new one—therefore, first of all, make
    sure to unpair the device from the old built-in “Bluetooth Devices” system if
    it’s previously been paired there.

6.  Right-click on the new Bluetooth Manager icon and select Options, then go
    to the “Assistant” tab and ensure “Hide” is checked in the middle panel (this
    prevents annoying notifications at the bottom right of the screen)

7.  Use the new Bluetooth Manager to pair the device.

8.  Use DSISelector.exe to select the COM port that this has created (see
    below).

9.  The first time you open a connection to a particular headset, a dialog
    will pop up asking for confirmation. Leave the “share authentication…” option
    ticked and say OK.  In future this should not show up.

10. In subsequent sessions, always plug the Toshiba dongle into the same USB
    port that you used during its initial setup.

11. To reduce the number of interruptions in data streaming, use a USB
    extension cable to plug the Bluetooth dongle into the computer, and then
    raise the dongle so that it is not shielded by the body of the computer.


Wired mode:

1.  You just need to install the Virtual COM Port (VCP) driver from
    https://www.silabs.com/products/mcu/Pages/USBtoUARTBridgeVCPDrivers.aspx 
    and then plug in.  Sometimes you may need to power-cycle the headset when
    switching between wired and wireless modes. Often you will not even need to
    do that.


Assuming the above setup has been completed, you can use DSISelector.exe to
switch between headsets, or to switch between wired and wireless mode for a
given headset: just wait for it to complete its inventory of COM ports, then
enter the number for the COM port you want.  The tool will save your
preference in the Windows environment variable “DSISerialPort”, and this
setting will then be used as the default by any software that uses the
DSI_API (e.g., by BCI2000) although not by DSIStreamer.

If you set the DSISerial variable (either using the DSISelector tool or
manually via System Properties->Advanced->Environment Variables), then
BCI2000 will automatically connect to this port and retrieve the headset's
details whenever it is launched.  If you do not do this, you will have to
enter the correct COMxx code in BCI2000's Config dialog, in the "Source"
tab, in the parameter named "DSISerialPort", and also configure the
channel names yourself. It is *much* easier to use the environment variable.