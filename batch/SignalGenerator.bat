#!/usr/bin/env DYLD_LIBRARY_PATH=../prog ../prog/BCI2000Shell#
@cls & "%~0\..\..\bootstrap\RunBCI2000Script.bat" %0 %* #!

###############  Windows-specific hacks #####################

system taskkill /F /FI "IMAGENAME eq DSISerial.exe"
system taskkill /F /FI "IMAGENAME eq SignalGenerator.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq DummySignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq FieldTripBuffer.exe"
system taskkill /F /FI "IMAGENAME eq P3SignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq SpectralSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"
system taskkill /F /FI "IMAGENAME eq CursorTask.exe"
system taskkill /F /FI "IMAGENAME eq P3Speller.exe"
system taskkill /F /FI "IMAGENAME eq StimulusPresentation.exe"

#############################################################

change directory $BCI2000LAUNCHDIR
show window
set title ${extract file base $0}
reset system
startup system localhost

start executable SignalGenerator       --local
start executable DummySignalProcessing --local
start executable DummyApplication      --local

wait for connected 600

set parameter SourceMin               -500muV
set parameter SourceMax                500muV
set parameter VisualizeSourceDecimation  1
set parameter VisualizeSource            1
set parameter VisualizeTiming            1
set parameter HighPassFilter             0
set parameter LowPassFilter              0

set parameter NoiseAmplitude           1000muV

set parameter Source:Signal%20Properties float   SamplingRate=           300Hz    // sample rate
set parameter Source:Signal%20Properties int     SampleBlockSize=         12      // number of samples transmitted at a time (9 = 30ms, 12 = 40ms, 15 = 50ms)
set parameter Source:Signal%20Properties list    ChannelNames=    21      P3  C3  F3  Fz  F4  C4  P4  Cz  Pz Fp1 Fp2  T7  P7  O1  O2  F7  F8  A2  P8  T8 TRG // the same channel order, but using more accurate names for our record
set parameter Source:Online%20Processing list    TransmitChList=  21       1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21 // 
set parameter Source:Signal%20Properties list    SourceChOffset=  21       0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0 // 
set parameter Source:Signal%20Properties list    SourceChGain=    21       1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1 // 
set parameter Source:Signal%20Properties int     SourceCh=        21              // number of digitized and stored channels


setconfig

