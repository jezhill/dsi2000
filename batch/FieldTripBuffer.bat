#!/usr/bin/env DYLD_LIBRARY_PATH=../prog ../prog/BCI2000Shell#
@cls & "%~0\..\..\bootstrap\RunBCI2000Script.bat" %0 %* #!

###############  Windows-specific hacks #####################

system taskkill /F /FI "IMAGENAME eq DSISerial.exe"
system taskkill /F /FI "IMAGENAME eq SignalGenerator.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq DummySignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq FieldTripBuffer.exe"
system taskkill /F /FI "IMAGENAME eq P3SignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq SpectralSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"
system taskkill /F /FI "IMAGENAME eq CursorTask.exe"
system taskkill /F /FI "IMAGENAME eq P3Speller.exe"
system taskkill /F /FI "IMAGENAME eq StimulusPresentation.exe"

#############################################################

change directory $BCI2000LAUNCHDIR
show window
set title ${extract file base $0}
reset system
startup system localhost

set environment SRC DSISerial
#set environment SRC SignalGenerator

start executable $SRC             --local
start executable FieldTripBuffer  --local
start executable DummyApplication --local

wait for connected 600

if [ $SRC == SignalGenerator ]

	set parameter ModulateAmplitude 1
	set parameter SineChannelX 1
	set parameter SineChannelY 2
	
	setconfig
	
else

	set parameter VisualizeImpedances  0
	set parameter ImpedanceDriverOn    0
	set parameter SourceMin         -250muV
	set parameter SourceMax          250muV

	if [ ${get parameter DSISerialPort} != "" ]; setconfig; end
	
end
