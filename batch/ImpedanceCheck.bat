#!/usr/bin/env DYLD_LIBRARY_PATH=../prog ../prog/BCI2000Shell#
@@cls & "%~dp0..\bootstrap\RunBCI2000Script.bat" %0 %* #!

###############  Windows-specific hacks #####################

system taskkill /F /FI "IMAGENAME eq DSISerial.exe"
system taskkill /F /FI "IMAGENAME eq SignalGenerator.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq DummySignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq FieldTripBuffer.exe"
system taskkill /F /FI "IMAGENAME eq P3SignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq SpectralSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"
system taskkill /F /FI "IMAGENAME eq CursorTask.exe"
system taskkill /F /FI "IMAGENAME eq P3Speller.exe"
system taskkill /F /FI "IMAGENAME eq StimulusPresentation.exe"

#############################################################

change directory $BCI2000LAUNCHDIR
show window
set title ${extract file base $0}
reset system
startup system localhost

start executable DSISerial             --local --FileFormat=Null
start executable DummySignalProcessing --local
start executable DummyApplication      --local

wait for connected 600

# the following parameter files shouldn't be necessary provided the
# DSISerialPort environment variable is pre-configured to the right COM#
#load parameterfile ../parms/DSI24-IncludingTriggerChannel.prm
#load parameterfile ../parms/DSI-ImpedanceMeasurement.prm

set script OnSuspend   "quit"
set script OnSetConfig "set state Running 1"
set button 1 "Analog Reset"           "set state HeadsetAnalogReset 1"

set parameter SourceMin               -250muV
set parameter SourceMax                250muV
set parameter VisualizeSourceDecimation  1
set parameter VisualizeSource            1
set parameter VisualizeTiming            1
set parameter HighPassFilter             0
set parameter LowPassFilter              0

set parameter VisualizeImpedances        1
set parameter ImpedanceDriverOn          1
if [ ${get parameter DSISerialPort} != "" ]; setconfig; end
