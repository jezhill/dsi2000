#!/usr/bin/env DYLD_LIBRARY_PATH=../prog ../prog/BCI2000Shell#
@cls & "%~0\..\..\bootstrap\RunBCI2000Script.bat" %0 %* #!

###############  Windows-specific hacks #####################

system taskkill /F /FI "IMAGENAME eq DSISerial.exe"
system taskkill /F /FI "IMAGENAME eq SignalGenerator.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq DummySignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq FieldTripBuffer.exe"
system taskkill /F /FI "IMAGENAME eq P3SignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq SpectralSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"
system taskkill /F /FI "IMAGENAME eq CursorTask.exe"
system taskkill /F /FI "IMAGENAME eq P3Speller.exe"
system taskkill /F /FI "IMAGENAME eq StimulusPresentation.exe"

#############################################################

change directory $BCI2000LAUNCHDIR
show window
set title ${Extract file base $0}
reset system
startup system localhost

start executable DSISerial          --local
start executable P3SignalProcessing --local
start executable P3Speller          --local

wait for connected 600

set button 1 "Analog Reset"   "set state HeadsetAnalogReset 1"

load parameterfile ../parms/ClassicCopySpelling.prm

#setconfig
