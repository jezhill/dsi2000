#!/bin/sh
set -e
export appname="RunShellScriptInPlace.app"
osacompile -o "$appname" <<EOF
on run
	tell application "Finder" to set appName to ((the name of (the path to me)) as text)
	display dialog ((quoted form of appName) & " should be launched by dragging and dropping a script file onto it, or by double-clicking a script file that has been associated with this application.") buttons {"OK"}
end run
on open someDroppedAliases
	tell application "Finder" to set appName to ((the name of (the path to me)) as text)
	set argc to the number of items of someDroppedAliases
	if argc is 1 then
		set batFile to the first item of someDroppedAliases
		tell application "Finder"
			set parentDir to the container of batFile
			set batFileName to the name of batFile
		end tell
		set parentDir to the quoted form of the POSIX path of (parentDir as text)
		set batFileName to the quoted form of ("./" & (batFileName as text))
		set joiner to " && "
		set asynchronousFinish to " &>/dev/null &"
		set theScript to ""
		set theScript to theScript & "cd " & parentDir
		set theScript to theScript & joiner
		set theScript to theScript & "chmod 777 " & batFileName
		set theScript to theScript & joiner
		set theScript to theScript & batFileName
		set theScript to theScript & asynchronousFinish
		do shell script theScript
	else
		display dialog (appName & " cannot open more than one script at once.") buttons {"OK"}
	end if
end open
EOF

cat <<EOF

Droplet $appname has been created here.

You can drag .bat files onto it to launch them.

Alternatively, make a .bat file launch when double-clicked, as follows:
(1) In the Finder, use the "Get Info" option on any .bat file
(2) Expand the "Open with:" section of the Info window, and associate the file with $appname
(3) Click "Change All..."  to make other similar files open in $appname
EOF

